		HTML Viewer
		-----------

When viewing HTML files, Geomview checks environment variable
$WEBBROWSER and uses this value, if set.  If not set, the command
`sensible-browser' is used; see its manpage to learn how to configure
sensible-browser.


		PDF Viewer
		----------

When viewing PDF files, Geomview checks environment variable
$PDFVIEWER and uses this value, if set.  If not set, the command
`evince' is used.


		File Locations
		--------------

Data files are installed in /usr/share/geomview/data.

Mathematica ".m" files are installed in /usr/share/geomview/mathematica.

Modules are installed in /usr/lib/geomview; modules that require XForms
are not included.  A couple of the modules have manpages, which can
be found in /usr/share/doc/geomview.  Code for example modules is
located in /usr/share/doc/geomview/examples.


		Dump Core Now?
		--------------

If something goes terribly wrong, geomview asks whether to 
stop with a message such as:

  > geomview tetra
  Geomview: internal error: Illegal instruction; dump core now (y/n) [n] ?

One common cause for this is if the OpenGL libs are missing, damaged,
or mismatched, e.g.

  http://bugs.debian.org/cgi-bin/bugreport.cgi?archive=no&bug=105488

Please check that you can indeed run some other open GL application,
such as glxinfo or glxgears before reporting a problem.


 -- Steve M. Robbins <smr@debian.org>, Sun,  7 Jun 2015 22:22:14 -0500
